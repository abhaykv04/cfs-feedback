function createTotalChart(service, start, end) {
    $('#total-chart').kendoChart({
        dataSource: {
            transport: {
                read:
                    'http://localhost:3000/totalratings?service=' +
                    service +
                    '&start=' +
                    start +
                    '&end=' +
                    end,
                dataType: 'json'
            }
        },
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: 'line'
        },
        series: [
            {
                field: 'total',
                categoryField: 'date'
            }
        ],
        valueAxis: {
            majorUnit: 1
        },
        tooltip: {
            visible: true
        }
    });
}

function createExceptionChart(service, start, end) {
    $('#exception-chart').kendoChart({
        dataSource: {
            transport: {
                read:
                    'http://localhost:3000/exceptionratings?service=' +
                    service +
                    '&start=' +
                    start +
                    '&end=' +
                    end,
                dataType: 'json'
            }
        },
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: 'line'
        },
        series: [
            {
                field: 'exception',
                categoryField: 'date'
            }
        ],
        valueAxis: {
            majorUnit: 1
        },
        tooltip: {
            visible: true
        }
    });
}

function createDetailsGrid(service, start, end) {
    $('#details-grid').kendoGrid({
        dataSource: {
            transport: {
                read:
                    'http://localhost:3000/ratingdetails?service=' +
                    service +
                    '&start=' +
                    start +
                    '&end=' +
                    end,
                dataType: 'json'
            },
            schema: {
                model: {
                    fields: {
                        mrid: { type: 'string' },
                        date: { type: 'string' },
                        rating: { type: 'number' }
                    }
                }
            },
            pageSize: 20
        },
        height: 400,
        scrollable: true,
        sortable: true,
        filterable: true,
        pageable: {
            input: true,
            numeric: false
        },
        columns: [
            { field: 'mrid', title: 'MRID' },
            { field: 'date', title: 'Submitted On' },
            { field: 'rating', title: 'Rating' }
        ]
    });
}

function constructServiceDropdown() {
    // Get services array
    $.getJSON('http://localhost:3000/services', (json) => {
        // Get service dropdown reference
        const serviceDropdown = $('#service-dropdown');

        // For each service in array
        json.forEach((service) => {
            // Append an item into dropdown
            serviceDropdown.append(
                '<a class="dropdown-item" href="#/' +
                    service +
                    '">' +
                    service +
                    '</a>'
            );
        });
    });
}

function getDefaultDates() {
    // Get current date
    const currentDate = new Date();

    // Convert current date into local date
    let currentLocalDate = new Date(
        currentDate.getTime() - currentDate.getTimezoneOffset() * 60000
    );

    // Initialise end date
    const endDate = currentLocalDate.toISOString().split('T')[0];

    // Initialise start date
    const startDate = new Date(
        currentLocalDate.setDate(currentLocalDate.getDate() - 6)
    )
        .toISOString()
        .split('T')[0];

    // Return start and end dates
    return [startDate, endDate];
}

function createCharts(service, startDate, endDate) {
    createTotalChart(service, startDate, endDate);
    createExceptionChart(service, startDate, endDate);
    createDetailsGrid(service, startDate, endDate);
}

$(document).ready(() => {
    /**
     * Calculate default values
     */

    // Construct service dropdown
    constructServiceDropdown();

    // Set default service name
    let service = 'Overall';

    // Get default dates
    const dates = getDefaultDates();

    // Initialise start and end dates
    let startDate = dates[0];
    let endDate = dates[1];

    // Set default dates
    $('#start-date').val(startDate);
    $('#end-date').val(endDate);

    /**
     * Add router
     */

    // Create new Kendo Router
    const router = new kendo.Router();

    // Route handler for /
    router.route('/', () => {
        router.navigate('/Overall');
    });

    // Route handler for /:service
    router.route('/:service', (service) => {
        // Change dropdown button text
        $('#service-dropdown-button').html('Service: ' + service);

        // Create charts for respective service
        createCharts(service, startDate, endDate);
    });

    // Start router
    router.start();

    /**
     * Add jQuery 'on' events
     */

    // Submit button on-click event
    $('#submit-button').on('click', () => {
        // Get start and end date
        startDate = $('#start-date').val();
        endDate = $('#end-date').val();

        if (!startDate) {
            alert('Invalid "From" Date');
        } else if (!endDate) {
            alert('Invalid "To" Date');
        } else {
            // Create chart and grid with new values
            createCharts(service, startDate, endDate);
        }
    });
});

$(document).bind('kendo:skinChange', createTotalChart);
$(document).bind('kendo:skinChange', createExceptionChart);
$(document).bind('kendo:skinChange', createDetailsGrid);
