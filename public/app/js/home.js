function createAvgChart() {
    $('#week-avg-chart').kendoChart({
        dataSource: {
            transport: {
                read: 'http://localhost:3000/weekaverage',
                dataType: 'json'
            }
        },
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: 'line'
        },
        series: [
            {
                field: 'average',
                categoryField: 'date'
            }
        ],
        valueAxis: {
            majorUnit: 1
        },
        tooltip: {
            visible: true,
            template: '#= value #/5'
        }
    });
}

function createExceptionsChart() {
    $('#week-exceptions-chart').kendoChart({
        dataSource: {
            transport: {
                read: 'http://localhost:3000/weekexceptions',
                dataType: 'json'
            }
        },
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: 'line'
        },
        series: [
            {
                field: 'exceptions',
                categoryField: 'date'
            }
        ],
        valueAxis: {
            majorUnit: 1
        },
        tooltip: {
            visible: true
        }
    });
}

function createIsCompleteChart() {
    $('#iscomplete-chart').kendoChart({
        dataSource: {
            transport: {
                read: 'http://localhost:3000/iscomplete',
                dataType: 'json'
            }
        },
        legend: {
            position: 'bottom'
        },
        seriesDefaults: {
            type: 'pie'
        },
        series: [
            {
                field: 'value',
                categoryField: 'category'
            }
        ],
        tooltip: {
            visible: true,
            template: '#= category #: #= value #%'
        }
    });
}

$(document).ready(() => {
    createAvgChart();
    createExceptionsChart();
    createIsCompleteChart();
});

$(document).bind('kendo:skinChange', createAvgChart);
$(document).bind('kendo:skinChange', createExceptionsChart);
$(document).bind('kendo:skinChange', createIsCompleteChart);
