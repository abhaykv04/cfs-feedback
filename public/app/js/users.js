function createUsersChart(start, end) {
    $('#users-chart').kendoChart({
        dataSource: {
            transport: {
                read:
                    'http://localhost:3000/numofusers?start=' +
                    start +
                    '&end=' +
                    end,
                dataType: 'json'
            }
        },
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: 'line'
        },
        series: [
            {
                field: 'users',
                categoryField: 'date'
            }
        ],
        valueAxis: {
            majorUnit: 1
        },
        tooltip: {
            visible: true
        }
    });
}

function createNotCompletedChart(start, end) {
    $('#not-completed-chart').kendoChart({
        dataSource: {
            transport: {
                read:
                    'http://localhost:3000/notcompleted?start=' +
                    start +
                    '&end=' +
                    end,
                dataType: 'json'
            }
        },
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: 'line'
        },
        series: [
            {
                field: 'users',
                categoryField: 'date'
            }
        ],
        valueAxis: {
            majorUnit: 1
        },
        tooltip: {
            visible: true
        }
    });
}

function createDetailsGrid(start, end) {
    $('#details-grid').kendoGrid({
        dataSource: {
            transport: {
                read:
                    'http://localhost:3000/userdetails?start=' +
                    start +
                    '&end=' +
                    end,
                dataType: 'json'
            },
            schema: {
                model: {
                    fields: {
                        mrid: { type: 'string' },
                        date: { type: 'string' },
                        iscomplete: { type: 'string' }
                    }
                }
            },
            pageSize: 20
        },
        height: 400,
        scrollable: true,
        sortable: true,
        filterable: true,
        pageable: {
            input: true,
            numeric: false
        },
        columns: [
            { field: 'mrid', title: 'MRID' },
            { field: 'date', title: 'Submitted On' },
            { field: 'iscomplete', title: 'Completed Survey' }
        ]
    });
}

function getDefaultDates() {
    // Get current date
    const currentDate = new Date();

    // Convert current date into local date
    let currentLocalDate = new Date(
        currentDate.getTime() - currentDate.getTimezoneOffset() * 60000
    );

    // Initialise end date
    const endDate = currentLocalDate.toISOString().split('T')[0];

    // Initialise start date
    const startDate = new Date(
        currentLocalDate.setDate(currentLocalDate.getDate() - 6)
    )
        .toISOString()
        .split('T')[0];

    // Return start and end dates
    return [startDate, endDate];
}

function createCharts(startDate, endDate) {
    createUsersChart(startDate, endDate);
    createNotCompletedChart(startDate, endDate);
    createDetailsGrid(startDate, endDate);
}

$(document).ready(() => {
    // Get default dates
    const dates = getDefaultDates();

    // Initialise start and end dates
    let startDate = dates[0];
    let endDate = dates[1];

    // Set default dates
    $('#start-date').val(startDate);
    $('#end-date').val(endDate);

    // Create chart and grid with default values
    createCharts(startDate, endDate);

    // Submit button on-click event
    $('#submit-button').on('click', () => {
        // Get start and end date
        startDate = $('#start-date').val();
        endDate = $('#end-date').val();

        if (!startDate) {
            alert('Invalid "From" Date');
        } else if (!endDate) {
            alert('Invalid "To" Date');
        } else {
            // Create chart and grid with new dates
            createCharts(startDate, endDate);
        }
    });
});

$(document).bind('kendo:skinChange', createUsersChart);
$(document).bind('kendo:skinChange', createNotCompletedChart);
$(document).bind('kendo:skinChange', createDetailsGrid);
