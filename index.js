// Create express app instance
const express = require('express');
const app = express();

// Require tedious module
const Connection = require('tedious').Connection;
const Request = require('tedious').Request;
const TYPES = require('tedious').TYPES;

// SQL Server config object
const config = {
    userName: 'abhay',
    password: 'kumar',
    server: 'localhost',
    options: {
        database: 'CFSFeedback'
    }
};

// Server static files from public folder
app.use(express.static('public'));

// Enable CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
});

// GET method for requests at /
app.get('/', (req, res) => {
    // Redirect to home page
    res.redirect('/home');
});

// GET method for requests at /home
app.get('/home', (req, res) => {
    // Send home.html
    res.sendFile('home.html', { root: __dirname + '/public' });
});

// GET method for requests at /users
app.get('/users', (req, res) => {
    // Send users.html
    res.sendFile('users.html', { root: __dirname + '/public' });
});

// GET method for requests at /ratings
app.get('/ratings', (req, res) => {
    // Send ratings.html
    res.sendFile('ratings.html', { root: __dirname + '/public' });
});

// GET method for requests at /weekaverage
app.get('/weekaverage', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];
        const query = 'select * from WeekAverage';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare a temporary object
            let tempObject = {};

            // Create date array from SubmittedOn column
            const dateArray = String(columns[0].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign AverageRating column value to average property
            tempObject.average = columns[1].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /weekexceptions
app.get('/weekexceptions', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];
        const query =
            'select * from ExceptionRatings where SubmittedOn > GETDATE() - 7';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare a temporary object
            let tempObject = {};

            // Create date array from SubmittedOn column
            const dateArray = String(columns[1].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign TotalExceptions column value to exceptions property
            tempObject.exceptions = columns[2].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /iscomplete
app.get('/iscomplete', (req, res) => {
    // Create a new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and helper object
        let helperObject = {
            yes: 0,
            no: 0,
            total: 0
        };
        const query = 'select * from IsComplete';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned');

            // Close connection
            connection.close();

            // Initialise response
            const responseArray = [
                {
                    category: 'Yes',
                    value: Math.round(
                        (helperObject.yes / helperObject.total) * 100
                    )
                },
                {
                    category: 'No',
                    value: Math.round(
                        (helperObject.no / helperObject.total) * 100
                    )
                }
            ];

            // Send response as json
            res.json(responseArray);
        });

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // If column has value, increment yes property, else increment no property
            columns[0].value ? helperObject.yes++ : helperObject.no++;

            // Increment total property, everytime
            helperObject.total++;
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /numofusers
app.get('/numofusers', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];

        // Use parameters to prevent injection
        const query =
            'select * from NoOfUsers where SubmittedOn between @start and @end';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Add paramaters to request
        request.addParameter('start', TYPES.VarChar, req.query.start);
        request.addParameter('end', TYPES.VarChar, req.query.end);

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare temporary object
            let tempObject = {};

            // Create a date array from SubmittedOn column
            const dateArray = String(columns[0].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign NoOfUsers column value to users property
            tempObject.users = columns[1].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /notcompleted
app.get('/notcompleted', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];

        // Use parameters to prevent injection
        const query =
            'select * from NotCompleted where SubmittedOn between @start and @end';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Add paramaters to request
        request.addParameter('start', TYPES.VarChar, req.query.start);
        request.addParameter('end', TYPES.VarChar, req.query.end);

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare temporary object
            let tempObject = {};

            // Create a date array from SubmittedOn column
            const dateArray = String(columns[0].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign NoOfUsers column value to users property
            tempObject.users = columns[1].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /userdetails
app.get('/userdetails', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];

        // Use parameters to prevent injection
        const query =
            'select * from UserDetails where SubmittedOn between @start and @end';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Add parameters to request
        request.addParameter('start', TYPES.VarChar, req.query.start);
        request.addParameter('end', TYPES.VarChar, req.query.end);

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare a temporary object
            let tempObject = {};

            // Assign MRID column value to mrid property
            tempObject.mrid = columns[0].value;

            // Create date array from SubmittedOn column
            const dateArray = String(columns[1].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign iscomplete property based on isComplete column value
            tempObject.iscomplete = columns[2].value ? 'Yes' : 'No';

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /services
app.get('/services', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];
        const query = 'select * from ServiceNames order by ServiceName';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send resopnse as json
            res.json(responseArray);
        });

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Push column value into response array
            responseArray.push(columns[0].value);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /totalratings
app.get('/totalratings', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and reponse
        let responseArray = [];

        // Use parameters to prevent injection
        const query =
            'select * from TotalRatings where ServiceName = @service and SubmittedOn between @start and @end';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Add parameters to request
        request.addParameter('service', TYPES.VarChar, req.query.service);
        request.addParameter('start', TYPES.VarChar, req.query.start);
        request.addParameter('end', TYPES.VarChar, req.query.end);

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare a temporary object
            let tempObject = {};

            // Create date array from SubmittedOn column
            const dateArray = String(columns[1].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign TotalRating column value to total property
            tempObject.total = columns[2].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /exceptionratings
app.get('/exceptionratings', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and reponse
        let responseArray = [];

        // Use parameters to prevent injection
        const query =
            'select * from ExceptionRatings where ServiceName = @service and SubmittedOn between @start and @end';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Add parameters to request
        request.addParameter('service', TYPES.VarChar, req.query.service);
        request.addParameter('start', TYPES.VarChar, req.query.start);
        request.addParameter('end', TYPES.VarChar, req.query.end);

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare a temporary object
            let tempObject = {};

            // Create date array from SubmittedOn column
            const dateArray = String(columns[1].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign ExceptionRating column value to exception property
            tempObject.exception = columns[2].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// GET method for requests at /ratingdetails
app.get('/ratingdetails', (req, res) => {
    // Create new Connection object
    const connection = new Connection(config);

    // Emit connect event
    connection.on('connect', (err) => {
        // Log error, if any
        if (err) return console.error(err);

        // Connection established
        console.log('Connected');
        executeQuery();
    });

    function executeQuery() {
        // Initialise query and response
        let responseArray = [];

        // Use parameters to prevent injection
        const query =
            'select * from RatingDetails where ServiceName = @service and SubmittedOn between @start and @end';

        // Create new Request object using query
        request = new Request(query, (err, rowCount) => {
            // Log error, if any
            if (err) return console.error(err);

            // Log number of rows returned
            console.log(rowCount + ' row(s) returned.');

            // Close connection
            connection.close();

            // Send response as json
            res.json(responseArray);
        });

        // Add parameters to request
        request.addParameter('service', TYPES.VarChar, req.query.service);
        request.addParameter('start', TYPES.VarChar, req.query.start);
        request.addParameter('end', TYPES.VarChar, req.query.end);

        // Emit row event for each row in result
        // A row is an array of columns (objects)
        request.on('row', (columns) => {
            // Declare a temporary object
            let tempObject = {};

            // Assign MRID column value to mrid property
            tempObject.mrid = columns[1].value;

            // Create date array from SubmittedOn column
            const dateArray = String(columns[2].value).split(' ');

            // Construct date property
            tempObject.date = dateArray[1] + ' ' + dateArray[2];

            // Assign Rating column value to rating property
            tempObject.rating = columns[3].value;

            // Push object into response array
            responseArray.push(tempObject);
        });

        // Execute SQL request
        connection.execSql(request);
    }
});

// 404 handler
app.use((req, res) => res.status(404).send('<h1>404: Not Found</h1>'));

// Start server
app.listen(3000, () => console.log('Listening on port 3000...'));
